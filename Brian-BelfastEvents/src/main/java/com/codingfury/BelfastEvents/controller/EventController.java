package com.codingfury.BelfastEvents.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestTemplate;

import com.codingfury.BelfastEvents.model.AllEvents;
import com.codingfury.BelfastEvents.model.Event;
import com.codingfury.BelfastEvents.service.ApiEventService;


@Controller
@RequestMapping("/")

public class EventController {

	@Autowired
	ApiEventService apiEventService;
	private static AllEvents allEvents;
	
	@GetMapping("/")
    public String home(Model model) {
		model.addAttribute("pageTitle", "Belfast Events!");
		RestTemplate restTemplate = new RestTemplate();
		this.allEvents = restTemplate.getForObject("http://neueda-flask-bndouglas.c9users.io/belfast-events/api/", AllEvents.class);
		//ArrayList<Event> allEvents = apiEventService.getEvents();
		model.addAttribute("events", allEvents.getAllEvents());
		return "index";
	}
	
	
	@GetMapping("/{identifier}")
	public String event(@PathVariable String identifier,  Model model) {
		
		model.addAttribute("pageTitle", "Belfast Events!");
		Event selectedEvent = this.allEvents.getEventWithId(identifier);
		String selectedEventTitle = selectedEvent.getTitle();
		model.addAttribute("event", selectedEvent);
		return "eventPage";
	}
	
	
	
	
}

