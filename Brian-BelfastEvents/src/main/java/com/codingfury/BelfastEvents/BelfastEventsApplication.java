package com.codingfury.BelfastEvents;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BelfastEventsApplication {

	public static void main(String[] args) {
		SpringApplication.run(BelfastEventsApplication.class, args);
	}
}


