package com.codingfury.springweb.repositories;

import org.springframework.data.repository.CrudRepository;

import com.codingfury.springweb.model.Book;

public interface BookRepository extends CrudRepository<Book, Long> {

}
