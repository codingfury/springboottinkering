package com.codingfury.springweb.repositories;

import org.springframework.data.repository.CrudRepository;

import com.codingfury.springweb.model.Author;

public interface AuthorRepository extends CrudRepository<Author, Long> {
	
	

}
