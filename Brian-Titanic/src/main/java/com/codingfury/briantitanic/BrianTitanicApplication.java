package com.codingfury.briantitanic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BrianTitanicApplication {

	public static void main(String[] args) {
		SpringApplication.run(BrianTitanicApplication.class, args);
		
	}
	
}
